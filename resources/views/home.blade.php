@extends('layouts.app')

@section('scripts')
    <script src="{{ asset('js/panel.js') }}"></script>
@endsection

@section('content')
<div class="content">
    <div class="page-header">
        <h2>
            My Domains
        </h2>
    </div>
    <div class="boxes-container">
        <div class="box-container">
            <div class="box domain-box">
                <form method="POST" id="add-domain" action="{{ route('addDomain') }}" aria-label="{{ __('add_domain') }}">
                    @csrf
                    <div class="box-header">
                        <h4>
                            <i class="fas fa-globe"></i> Add New Domain
                        </h4>
                    </div>
                    <div class="box-content">
                        <div class="domain-group">
                            <div class="domain-lable">http://</div><input id="domain" type="text" class="default-input inline-input domain-input" name="domain" required>
                        </div>
                    </div>
                    <div class="box-footer">
                        <a href="#" onclick="saveDomain()" class="btn save-domain">Save me!</a>
                    </div>
                </form>
            </div>
        </div>
        <div class="box-container">
            <table class="domains-table">
                <thead>
                    <tr>
                        <th>
                            Domain Name
                        </th>
                        <th>
                            Secret
                        </th>
                        <th>
                            Verified
                        </th>
                        <th>

                        </th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($domains as $domain)
                        <tr>
                            <td>
                                {{ $domain->address }}
                            </td>
                            <td>
                                @if (!empty($domain->secret))
                                    {{ $domain->secret }}
                                @else
                                    Secret is not generated
                                @endif
                            </td>
                            @if ($domain->verified)
                                <td class="verified">VERIFIED</td>
                            @elseif ($domain->verified == false)
                                <td class="not-verified">DNS is set but not verified</td>
                            @else
                                <td class="not-verified">DNS is not set</td>
                            @endif

                            @if ($domain->verified)
                                <td class="success-status">VERIFIED</td>
                            @else
                                <td>
                                    <form method="POST" id="verify-domain-{{$domain->id}}" action="{{ route('verifyDomain') }}" aria-label="{{ __('verifyDomain') }}">
                                        @csrf
                                        <input type="hidden" value="{{$domain->id}}" name="domain-id" />
                                        <a href="#" onclick="verifyDomain('{{$domain->id}}')" class="verifiy-domain">Verify me!</a>
                                    </form>
                                </td>
                            @endif
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection
