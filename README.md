# domain-ownership-verifier

## What is this?

I tried to manage domains of a user which is mapped to our dns servers and verify them.

## How to use it?

First of all, you should have php 7.* installed.

1- run this command in the project's folder:
`composer install`

2- and then this:
`composer du`

3- now you should create the database (which name you should put in your .env file. I'll put my own .env file.) and then run command below:
`php artisan migrate`

4- now you can run the project happy ever after:
`php artisan serve`

## How it works?

This project is actually designed to be used in a could service provider's service to identiy a domain's ownership.

It uses to things:
*   checkes if the domains dns server is set to arvancloud
*   then it checks if the provided secret key is within domain's TXT dns records.
