<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Domain;
use Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        $userDomains = Domain::where('user_id', $user->id)->get();
        return view('home', ['domains' => $userDomains]);
    }

    public function addDomain(Request $request) {
        $user = Auth::user();
        $address = $request->input('domain');
        $userId = $user->id;
        $domain = new Domain;
        $domain->address = $address;
        $domain->user_id = $userId;
        $domain->secret = sha1($address . time());
        $splittedAddresses = explode('.', $address);
        $extention = $splittedAddresses[1];
        if (count($splittedAddresses) > 2) {
            $extention = $splittedAddresses[1] . $splittedAddresses[2];
        }
        $domain->extention = $extention;
        $domain->verified = NULL;
        $domain->save();
        return redirect()->route('home');
    }

    public function verifyDomain(Request $request) {
        $domainId = $request->input('domain-id');
        $domain = Domain::find($domainId);
        $verifyResult = $domain->verify();
        return redirect()->route('home');
    }
}
