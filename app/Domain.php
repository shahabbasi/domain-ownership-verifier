<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Domain extends Model
{
    protected $table = 'domains';
    protected $fillable = ['address', 'extention', 'verified', 'secret', 'user_id'];
    const OUR_DNS_SERVERS = [ 'g.ns.arvancdn.com', 'v.ns.arvancdn.com' ];

    public function verify() {
        $dnsResult = dns_get_record($this->address, DNS_NS);
        $dnsServers = [];
        foreach ($dnsResult as $record) {
            $dnsServers[] = $record['target'];
        }
        $dnsMatchStatus = false;
        foreach ($dnsServers as $dnsServer) {
            if (in_array($dnsServer, self::OUR_DNS_SERVERS)) {
                $dnsMatchStatus = true;
                $this->verified = false;
                $this->save();
            }
        }
        if (!$dnsMatchStatus) {
            return false;
        }
        $txtResults = dns_get_record($this->address, DNS_TXT);
        $txtRecords = [];
        foreach ($txtResults as $record) {
            $txtRecords[] = $record['entries'][0];
        }
        if (in_array($this->secret, $txtRecords)) {
            $this->verified = true;
            $this->save();
            return true;
        }
        return false;
    }
}
