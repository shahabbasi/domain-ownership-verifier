<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DnsRecord extends Model
{
    protected $table = 'domains';
    protected $fillable = ['domain_id', 'type', 'name', 'value'];
}
